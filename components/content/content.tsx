import { Config, DistanceBetween, Note } from "@/service/config";
import { AddPlayedNotes, NextNotes, Status } from "@/service/status";
import { Button, Flex, Grid, GridItem, Heading, Hide, Link } from "@chakra-ui/react";
import React, { useEffect, useState } from "react";
import Guitar, { getRenderFingerSpn } from "react-guitar";
import dark from "react-guitar-theme-dark";
import { standard } from "react-guitar-tunings";

const standardNotes = ["E", "B", "G", "D", "A", "E"];

const Content = (props: {
    status: Status;
    updateStatus: (status: Status) => void;
}) => {
    const status = props.status;
    const config = Config[status.level];
    let [nextNotes, setNextNotes] = useState<Note[]>([]);
    let [selectedNote, setSelectedNote] = useState("C");
    let [strings, setStrings] = useState<number[]>([0, 0, 0, 0, 0, 0]);

    useEffect(() => setNextNotes(NextNotes(status)), [status]);
    useEffect(() => setSelectedNote(nextNotes[0]), [nextNotes]);
    useEffect(() => {
        setStrings(
            standardNotes.map((note) => DistanceBetween(note, selectedNote))
        );
    }, [selectedNote]);

    const onNextClick = () => {
        props.updateStatus(AddPlayedNotes(status, nextNotes));
    };

    const onNoteClick = (event: any) => {
        setSelectedNote(event.target.innerHTML);
    };

    const notesJSX = () => {
        let jsx = nextNotes.map((note) => (
            <Link onClick={onNoteClick} key={note}>
                <Heading
                    color={note == selectedNote ? "gray.600" : "gray.400"}
                    size="2xl"
                    mr="0.5rem"
                    ml="0.5rem"
                >
                    {note}
                </Heading>
            </Link>
        ));
        return (
            <Flex direction="row" justifyContent="center" mb="1rem" mt="1rem">
                {jsx}
            </Flex>
        );
    };

    return (
        <Grid height="100%" templateColumns="auto" templateRows="10% 20% auto 20%">
            <GridItem></GridItem>
            <GridItem>
                <Flex
                    alignItems="center"
                    direction="column"
                    height="100%"
                    justifyContent="center"
                >
                    <Heading
                        width="80%"
                        mb="1rem"
                        color="gray.600"
                        size="2xl"
                        textAlign="center"
                    >
                        {config.help.mainText(status.bpm)}
                    </Heading>
                    <Heading color="gray.500" size="lg">
                        {config.help.subText}
                    </Heading>
                </Flex>
            </GridItem>
            <GridItem>
                <Flex
                    alignItems="center"
                    direction="column"
                    height="100%"
                    justifyContent="center"
                >
                    {notesJSX()}
                    <Hide below="sm">
                        <div style={{ fontSize: ".6vw" }}>
                            <Guitar
                                strings={strings}
                                frets={{ from: 0, amount: 12 }}
                                theme={dark}
                                renderFinger={getRenderFingerSpn(standard)}
                            />
                        </div>
                    </Hide>
                    <Button
                        mt="3rem"
                        backgroundColor="green.400"
                        color="white"
                        onClick={onNextClick}
                    >
                        Next
                    </Button>
                </Flex>
            </GridItem>
            <GridItem></GridItem>
        </Grid>
    );
};

export default Content;
