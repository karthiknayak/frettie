import { Flex, Heading, Link } from "@chakra-ui/react";

export default function Footer() {
  return (
    <Flex width="100vw" justifyContent={"center"} direction="row">
      <Heading size="sm">
        Made with by ❤️ by{" "}
        <Link color="blue.600" href="https://nayak.io">
          Karthik Nayak
        </Link>
      </Heading>
    </Flex>
  );
}
