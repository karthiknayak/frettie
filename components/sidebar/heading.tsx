import { Heading } from "@chakra-ui/react";

const SidebarHeading = (props: { title: string; color: string }) => {
  return (
    <Heading mb="0.2em" mt="0.2em" color={props.color} size="2xl">
      {props.title}
    </Heading>
  );
};

export default SidebarHeading;
