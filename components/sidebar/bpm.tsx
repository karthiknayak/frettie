import { Box, Button, Text } from "@chakra-ui/react"
import { useEffect, useRef, useState } from "react";

const BPM = (props: { bpm: number }) => {
	const acRef = useRef<AudioContext>();
	const [isPlaying, setIsPlaying] = useState(false);

	useEffect(() => {
		// Src: https://github.com/padenot/metro
		const ac = new AudioContext();
		let buf = ac.createBuffer(1, ac.sampleRate * 2, ac.sampleRate);
		let channel = buf.getChannelData(0);
		let phase = 0;
		let amp = 1;
		let duration_frames = ac.sampleRate / 50;
		const f = 330;
		for (var i = 0; i < duration_frames; i++) {
			channel[i] = Math.sin(phase) * amp;
			phase += 2 * Math.PI * f / ac.sampleRate;
			if (phase > 2 * Math.PI) {
				phase -= 2 * Math.PI;
			}
			amp -= 1 / duration_frames;
		}

		acRef.current = ac;

		let source = ac.createBufferSource();
		source.buffer = buf;
		source.loop = true;
		source.loopEnd = 1 / (props.bpm / 60);
		source.connect(ac.destination);
		source.start(0);

		// Disconnect from audioContext when component is cleaned
		return () => source.disconnect(ac.destination)
	}, [])

	const handleClick = () => {
		if (isPlaying && acRef.current) {
			acRef.current.suspend();
		} else if (acRef.current) {
			acRef.current.resume();

		}
		setIsPlaying(!isPlaying);
	}

	return <Box>
		<Button onClick={handleClick} size="md" colorScheme="green" variant="outline">
			<Text color="gray.600">
				{isPlaying ? "⏸" : "⏵"} {props.bpm} BPM
			</Text>
		</Button>
	</Box >
}

export default BPM;
