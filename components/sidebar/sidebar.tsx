import { MaxLevels } from "@/service/config";
import { Status } from "@/service/status";
import { Flex, Grid, GridItem } from "@chakra-ui/react";
import BPM from "./bpm";
import SidebarHeading from "./heading";

const Sidebar = (props: { status: Status }) => {
    let currentLevel = props.status.level;
    let colors = Array.apply(null, Array(MaxLevels)).map(function (x, i) {
        if (i + 1 == currentLevel) {
            return "yellow.400";
        } else if (i + 1 < currentLevel) {
            return "green.400";
        }
        return "gray.600";
    });

    return (
        <>
            <Grid height="100vh" templateColumns="auto" templateRows="auto 30% auto">
                <GridItem>
                    <Flex
                        height="100%"
                        justifyContent="center"
                        alignItems="center"
                        flexDirection="column"
                    >
                        {currentLevel > 1 && <BPM bpm={props.status.bpm} />}
                    </Flex>
                </GridItem>
                <GridItem>
                    <Flex
                        height="100%"
                        justifyContent="center"
                        alignItems="center"
                        flexDirection="column"
                    >
                        <SidebarHeading color={colors[0]} title="Level 1"></SidebarHeading>
                        <SidebarHeading color={colors[1]} title="Level 2"></SidebarHeading>
                        <SidebarHeading color={colors[2]} title="Level 3"></SidebarHeading>
                        <SidebarHeading color={colors[3]} title="Level 4"></SidebarHeading>
                        <SidebarHeading color={colors[4]} title="Level 5"></SidebarHeading>
                    </Flex>
                </GridItem>
                <GridItem></GridItem>
            </Grid>
        </>
    );
};

export default Sidebar;
