import { useEffect, useState } from "react";

const useLocalStorage = <T,>(key: string, fallbackValue: T) => {
  const [value, setValue] = useState(fallbackValue);

  useEffect(() => {
    if (value != fallbackValue) {
      localStorage.setItem(key, JSON.stringify(value));
    }
  }, [key, value, fallbackValue]);

  useEffect(() => {
    const stored = localStorage.getItem(key);
    setValue(stored ? JSON.parse(stored) : fallbackValue);
  }, [key, fallbackValue]);

  return [value, setValue] as const;
};

export default useLocalStorage;
