import { Config, MaxLevels, Note } from "./config";

export interface Status {
  level: number;
  notesFinished: Note[];
  bpm: number;
  iteration: number;
}

export const NewStatus = (): Status => {
  return {
    level: 1,
    notesFinished: [],
    bpm: 40,
    iteration: 1,
  };
};

export const NextNotes = (status: Status): Note[] => {
  const config = Config[status.level];

  let notesRemaining = config.notes
    .filter((x) => !status.notesFinished.includes(x))
    .sort((a, b) => 0.5 - Math.random());
  return notesRemaining.slice(0, config.notesToPlay);
};

export const AddPlayedNotes = (status: Status, notes: Note[]): Status => {
  const config = Config[status.level];

  status.notesFinished = status.notesFinished.concat(...notes);
  const notesDelta = Math.abs(
    config.notes.length - status.notesFinished.length
  );
  if (notesDelta < config.notesToPlay) {
    status.iteration = status.iteration + 1;
    if (status.iteration > config.iteration) {
      status.level = status.level + 1;
      status.iteration = 1;

      if (status.level > MaxLevels) {
        status.level = 2;
        status.bpm = status.bpm + 5;
      }
    }
    status.notesFinished = [];
  }
  return status;
};
