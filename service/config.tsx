const baseNotes = ["A", "B", "C", "D", "E", "F", "G"] as const;
const accidentalNotes = ["A#", "C#", "D#", "E", "F#", "G#"] as const;
export const AllNotes = [
    "A",
    "A#",
    "B",
    "C",
    "C#",
    "D",
    "D#",
    "E",
    "F",
    "F#",
    "G",
    "G#",
] as const;
export type Note = typeof baseNotes[number] | typeof accidentalNotes[number];

type LevelConfig = {
    requiresMetronome: boolean;
    iteration: number;
    notes: Note[];
    help: { mainText: (bpm: number) => string; subText: string };
    notesToPlay: number;
};

export const MaxLevels = 5;

export const Config: Record<number, LevelConfig> = {
    1: {
        requiresMetronome: false,
        iteration: 2,
        notes: [...baseNotes] as Note[],
        help: {
            mainText: (bpm: number) => "Play the shown note through frets 1-12",
            subText: "Click next after 3 runs",
        },

        notesToPlay: 1,
    },
    2: {
        requiresMetronome: true,
        iteration: 1,
        notes: [...baseNotes] as Note[],
        help: {
            mainText: (bpm: number) =>
                `Using a metronome at ${bpm} BPM, play the note once per beat`,
            subText: "Click next after 3 runs",
        },
        notesToPlay: 1,
    },
    3: {
        requiresMetronome: true,
        iteration: 1,
        notes: [...AllNotes] as Note[],
        help: {
            mainText: (bpm: number) =>
                `Using a metronome at ${bpm} BPM, play the note (inludes accidentals) once per beat`,
            subText: "Click next after 3 runs",
        },
        notesToPlay: 1,
    },
    4: {
        requiresMetronome: true,
        iteration: 1,
        notes: [...AllNotes] as Note[],
        help: {
            mainText: (bpm: number) =>
                `Using a metronome at ${bpm} BPM, play the first shown note up and the second note down`,
            subText: "Click next after 3 runs",
        },
        notesToPlay: 2,
    },
    5: {
        requiresMetronome: true,
        iteration: 1,
        notes: [...AllNotes] as Note[],
        help: {
            mainText: (bpm: number) =>
                `Using a metronome at ${bpm} BPM, play the shown 7 notes in up, down, up, down, up, down, up manner`,
            subText: "Click next when you can play comfortably",
        },
        notesToPlay: 7,
    },
};

export const DistanceBetween = (a: string, b: string): number => {
    const map: Record<string, number> = {
        A: 0,
        "A#": 1,
        B: 2,
        C: 3,
        "C#": 4,
        D: 5,
        "D#": 6,
        E: 7,
        F: 8,
        "F#": 9,
        G: 10,
        "G#": 11,
    };

    let v1 = map[a];
    let v2 = map[b];

    let diff = v2 - v1;
    if (diff < 0) {
        diff = diff + 12;
    } else if (diff == 0) {
        diff = 12;
    }
    return diff;
};
