/* eslint-disable react/no-unescaped-entities */
import { ArrowBackIcon } from "@chakra-ui/icons";
import {
    AspectRatio, Flex,
    Heading,
    Link,
    Stack,
    Text
} from "@chakra-ui/react";
import { useRouter } from "next/router";

export default function Help() {
    const router = useRouter();

    return (
        <>
            <Flex height="100vh" alignItems="center" justifyContent="center">
                <Flex width="50vw" direction="column">
                    <Flex justifyContent="end"></Flex>
                    <Flex position="relative">
                        <Link>
                            <ArrowBackIcon
                                position="absolute"
                                top="-10"
                                right="0"
                                boxSize="2em"
                                color={"blue.500"}
                                onClick={() => router.back()}
                            />
                        </Link>
                        <Heading size={"2xl"} mb="10px">
                            Why?
                        </Heading>
                    </Flex>
                    <Stack spacing={3}>
                        <Text>
                            I've been learning guitar for a while now. While I know how to
                            play most chords, I never really put any effort to learn the
                            fretboard and the notes. This means my improvisation skills are 0.
                            So I recently was looking for ways to learn all the notes on the
                            fretboard. This would make moving around the fretboard a bit more
                            effortless. This is similar to how touch typing helps you get
                            better at typing.
                        </Text>
                        <Text>
                            While looking on Youtube for a way to learn the fretboard, I found
                            the following video
                        </Text>
                        <AspectRatio maxW="500px" ratio={16 / 9}>
                            <iframe
                                title="naruto"
                                src="https://www.youtube.com/embed/PJddQ6Q0UDo"
                                allowFullScreen
                            />
                        </AspectRatio>
                        <Text>
                            This website is built to help you learn the fretboard as suggested
                            by the video. Grab your guitar and get started!
                        </Text>
                    </Stack>
                </Flex>
            </Flex>
        </>
    );
}
