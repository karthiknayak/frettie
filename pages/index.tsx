import Footer from "../components/footer/footer";
import { QuestionIcon } from "@chakra-ui/icons";
import { Button, Flex, Heading, Link } from "@chakra-ui/react";

export default function Home() {
  return (
    <>
      <Flex height="96vh" alignItems="center" justifyContent="center">
        <Flex direction="column" position="relative">
          <Flex position="absolute" justifyContent="end" top="-2em" right="0">
            <Link href="/help">
              <QuestionIcon color={"blue.500"} />
            </Link>
          </Flex>
          <Heading size="4xl">Learn The</Heading>
          <Heading size="4xl">Fretboard</Heading>
          <Link href="learn" mt="30px">
            <Button colorScheme="blue" w="100%">
              Start
            </Button>
          </Link>
        </Flex>
      </Flex>
      <Flex>
        <Footer></Footer>
      </Flex>
    </>
  );
}
