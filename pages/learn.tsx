import Content from "@/components/content/content";
import Sidebar from "@/components/sidebar/sidebar";
import useLocalStorage from "@/service/localStorage";
import { NewStatus, Status } from "@/service/status";
import { ArrowBackIcon } from "@chakra-ui/icons";
import { Box, Grid, GridItem, Link } from "@chakra-ui/react";
import { useRouter } from "next/router";

const fallback = NewStatus();

const Learn = () => {
  const router = useRouter();
  const [status, setStatus] = useLocalStorage<Status>("user.status", fallback);

  const updateStatus = (status: Status) => {
    setStatus({ ...status });
  };

  return (
    <>
      <Link>
        <ArrowBackIcon
          position="absolute"
          top="2em"
          right="4em"
          boxSize="2em"
          color="blue.500"
          onClick={() => router.back()}
        />
      </Link>
      <Grid templateRows="auto" templateColumns="15% auto">
        <GridItem>
          <Box bg="gray.100">
            <Sidebar status={status} />
          </Box>
        </GridItem>
        <GridItem>
          <Content status={status} updateStatus={updateStatus}></Content>
        </GridItem>
      </Grid>
    </>
  );
};

export default Learn;
